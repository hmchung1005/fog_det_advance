
import tensorflow as tf

import sys
sys.path.append('/gisangdan/kans/ai/src/rain-num-fcst-v2')
import train_auto as trn_auto
from utils.inference_log import get_logger
import rn_num_inference as inf
from preprocessing import data_input_validation
from datetime import datetime
import preprocessing as pre
from tensorflow import keras
import os
def train(tm):
    try:
        log = get_logger(1, error_log=False, log_name='rain_num_train_info')
        error_log = get_logger(1, error_log=True, log_name='rain_num_train_error')
        #inf.add_hosts()
        log.info('train starts')
        log.info('tm model path {}'.format(tm.model_path))
        param = tm.param_info
        log.info('prams ->{}'.format(param))
        start = str(param['train_start_ticket'])
        end = str(param['train_end_ticket'])
        bkwd_time_steps = int((datetime.strptime(end, '%Y%m%d%H%M') - datetime.strptime(start, '%Y%m%d%H%M')).seconds/60)
        hsr_df = pre.get_hsr_df(end,5,bkwd_time_steps)
        lstm_model =trn_auto.main(hsr_df,param,tm.model_path)
        try:
            saving_path = tm.model_path + '/rain_num.h5'
            keras.models.save_model(lstm_model, saving_path)
            # lstm_model.save(tm.model_path + '/rain_num.h5')
            log.info('model saved : {}'.format(os.path.exists(saving_path)))
        except Exception as e:
            error_log.error('loading model error :{}'.format(e))
    except Exception as e:
        error_log.error('train error :{}'.format(e))

def init_svc(im):
    '''
    init_svc(im) : 추론 서비스 초기화

    labels
        im.features.get('y_value')
    params
        im.param_info
    nn_info
        im.nn_info
    model_path
        im.get_model_path()

    return 값을 inference 함수에서 params 으로 접근
        ex) return {"svc_config":svc_config, "estimator":estimator}
    '''
    log = get_logger(1, error_log=False, log_name='rain_num_init_info')
    error_log = get_logger(1, error_log=True, log_name='rain_num_init_error')
    model_path = im.model_path + '/rain_num.h5'
    log.info('model path ->{}'.format(model_path))
    log.info('model path exists : {}'.format(os.path.exists(model_path)))
    try:
        model = inf.create_model()
        model = inf.load_saved_model(model, model_path)
        log.info('model loaded')
    except Exception as e:
        error_log.error('model loading error :{}'.format(e))
    model_param = {'model_path': model_path, 'saved_model':model}
    param = im.param_info
    params = {**param, **model_param}
    return params

def inference(df, params, batch_size):
    log = get_logger(1, error_log=False, log_name='rain_num_inference_info')
    error_log = get_logger(1, error_log=True, log_name='rain_num_inference_error')
    # pwd의 하위 log, error_log폴더에 .log저장, fog_logging.py참조
    log.info('df : {}, params : {}'.format(df,params))
    ticket_df=df[0]
    ticketNo = ticket_df.tolist()[0]
    log.info('ticketNo : {}'.format(ticketNo))
    #inf.add_hosts()
    is_null = data_input_validation(ticketNo)
    if is_null:
        error_log.error('Input data has null')
        return False
    else:
        log.info('call_threadfunc start')
        call_threadfunc(ticketNo,params, log, error_log)
        log.info('inference end')
        return True
    # log.info('call_threadfunc start')
    # call_threadfunc(ticketNo, log, error_log)
    # log.info('inference end')
    # return True

def processing_after_call(args):
    ticketNo, params = args
    inf.inference_mainjob(str(ticketNo),params)

def call_threadfunc(ticketNo,params, log, error_log):
    from functools import partial
    from concurrent.futures import ThreadPoolExecutor
    pool = ThreadPoolExecutor(max_workers=1)
    log.info('prams ->{}'.format(params))
    try:
        args = (ticketNo,params)
        log.info('args ->{}'.format(args))
        future = pool.submit(processing_after_call,(args))
    except Exception as e:
        error_log.error('processing_after_call error : {}'.format(e))
    log.info('call_threadfunc end')
