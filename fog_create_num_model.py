import pandas as pd
import numpy as np
from fog_config import fog_create_model_info , gk2a_fog_img_attrs , fog_class , fog_character
import math
import os
import json
import tensorflow as tf
from tensorflow.keras import layers, models

def round_down(n, decimals=0):
    multiplier = 10 ** decimals
    return math.floor(n * multiplier) / multiplier

def find_threshold_color():
    diff_rgba_list = []
    diff_rgba_list.append(gk2a_fog_img_attrs.diff1_rgba[:-1])
    diff_rgba_list.append(gk2a_fog_img_attrs.diff2_rgba[:-1])
    diff_rgba_list.append(gk2a_fog_img_attrs.diff3_rgba[:-1])
    diff_rgba_list.append(gk2a_fog_img_attrs.diff4_rgba[:-1])
    diff_rgba_list.append(gk2a_fog_img_attrs.diff5_rgba[:-1])
    diff_rgba_list.append(gk2a_fog_img_attrs.diff6_rgba[:-1])
    rgb_np = np.array(diff_rgba_list) / 255
    avg_list = []
    for i in range(rgb_np.shape[0]):
        avg_list.append(rgb_np[i].mean())

    threshold = min([round_down(x , 1)  for x in avg_list])
    return threshold

def logic(key , value , length , width , rgb_threshold):
    amos_rgb_mean = sum(value[length][width]) / 3
    try:
        target = float(key.split("_")[2])
        if (amos_rgb_mean > rgb_threshold and target < fog_class.weak_fog_high_end) or (amos_rgb_mean < rgb_threshold and target > fog_class.weak_fog_high_end):
            return True
        else:
            return False
    except:
        return False


# 학습 데이터 샘플링 로직.
# 현재 로직. 1.  rgb 값이 낮으면서 시정거리가 낮으면 학습 데이터에서 제외
#           2.  rgb 값이 높으면서 시정거리가 높으면 학습 데이터에서 제외
#           결론 : 색깔이 있으면서 시정거리가 높거나 색깔이 없으면서 시정거리가 낮으면 노이즈 데이터로 인식하고 학습 하지 않음
def sampling_with_logic(num_model_data_dic ,length , width):
    # 높은 rgb 값 낮은 rgb 값 구분 기준 생성
    # r, g, b 따로 최소 수치들을 모아서 평균치 ----> 대략 0.2 정도 되는 상황
    rgb_threshold = find_threshold_color()
    sampling_num_model_data_dic = num_model_data_dic.copy()
    for k,v in num_model_data_dic.items():
        use_bool = logic(k , v , length , width , rgb_threshold)
        if not use_bool:
            del sampling_num_model_data_dic[k]
    return sampling_num_model_data_dic

def set_gpu(gpu_num):
    physical_devices = tf.config.experimental.list_physical_devices('GPU')   # 물리적 GPU리스트
    if physical_devices != []:
        tf.config.experimental.set_visible_devices(physical_devices[gpu_num], 'GPU')   # 3번 GPU만 사용(여러개 쓸거면 슬라이싱도 됨)
        tf.config.experimental.set_memory_growth(physical_devices[gpu_num], True)




def build_model(input_shape , optimizer , loss_func , learning_rate , dnn_1, dnn_2, dnn_3 ):
    if optimizer == "1":
        optimizer_v = tf.keras.optimizers.Adam(learning_rate = learning_rate)
    elif optimizer =="2":
        optimizer_v = tf.keras.optimizers.RMSprop(learning_rate = learning_rate)
    elif optimizer == "3":
        optimizer_v = tf.keras.optimizers.SGD(learning_rate = learning_rate)
    if loss_func =="1":
        loss_func_v = "mse"
    elif loss_func == "2":
        loss_func_v = "mae"
    elif loss_func == "3":
        loss_func_v = "mape"
    model = tf.keras.models.Sequential([
            tf.keras.layers.Flatten(input_shape=(input_shape)),
            tf.keras.layers.Dense(dnn_1, activation='relu'),  # 128
            tf.keras.layers.Dense(dnn_2 , activation='relu'),
            tf.keras.layers.Dense(dnn_3, activation='relu'),
            tf.keras.layers.Dense(1)])

    model.compile(optimizer=optimizer_v,
                loss=loss_func_v, # 이진분류 binary_crossentropy / 1-of-K categorical_crossentropy / mse
                metrics=['mae'])
    return model



def main(num_model_data_dic , train_data_params , log , error_log):
    # 기지 중심 픽셀로부터 세로 길이
    length = int(train_data_params["num_model_pixel_length"])
    # 기지 중심 픽셀로부터 가로 길이
    width =  int(train_data_params["num_model_pixel_width"])

    # 학습 데이터 샘플링 로직.
    # 현재 로직. 1.  rgb 값이 낮으면서 시정거리가 낮으면 학습 데이터에서 제외
    #           2.  rgb 값이 높으면서 시정거리가 높으면 학습 데이터에서 제외
    #           결론 : 색깔이 있으면서 시정거리가 높거나 색깔이 없으면서 시정거리가 낮으면 노이즈 데이터로 인식하고 학습 하지 않음
    sampled_data = sampling_with_logic(num_model_data_dic ,length , width)

    total_data_counts = len(sampled_data)

    np_y = np.array([float(x.split("_")[2]) for x in sampled_data.keys()] , dtype = "float32" )
    np_y = np.clip(np_y , 0, fog_character.maximum_vis_distance_clipping + 1000 )
    np_y = np_y[..., tf.newaxis]
    np_x = np.array([x for x in sampled_data.values()] , dtype = "float32")



    test_data_ratio = float(train_data_params["num_model_test_data_ratio"])
    es_patience = int(train_data_params["es_patience"])
    EPOCHS = int(train_data_params["epoch_num"])
    BATCH_SIZE =  int(train_data_params["batch_size_num"]) * 100


    if test_data_ratio < 1 and test_data_ratio > 0 :
        train_data_ratio = 1 - test_data_ratio
    else:
        train_data_ratio = 0.9


    train_data_counts = int(total_data_counts  * train_data_ratio)


    train_y = np_y[:train_data_counts]
    train_x = np_x[:train_data_counts]

    test_y = np_y[train_data_counts : ]
    test_x = np_x[train_data_counts : ]
    val_counts = int(len(test_y) / 2)

    val_y = test_y[:val_counts]
    val_x = test_x[:val_counts]

    test_y = test_y[val_counts:]
    test_x = test_x[val_counts:]


    ### 데이터 없으면 학습 중지
    if len(train_x) == 0:
        log.info(" \n\n\n there is zero data for train set -------------> no training \n\n\n")
    elif len(val_x) == 0:
        log.info(" \n\n\n thre is zero data for valid set -------------> no training \n\n\n")
    elif len(test_x) == 0:
        log.info(" \n\n\n thre is zero data for test set -------------> no training \n\n\n")
    else:
        callback = tf.keras.callbacks.EarlyStopping(monitor='val_loss', patience= es_patience)
        saving_model_path = train_data_params["model_path"]
        # 수치 모델에서는 로스 함수랑 옵티마이즈가 적용이 된다
        optimizer = str(train_data_params["optimizer"])
        loss_func = str(train_data_params["loss_func"])
        learning_rate = train_data_params["learning_rate"]
        dnn_1 = int(train_data_params["dnn_1"])
        dnn_2 = int(train_data_params["dnn_2"])
        dnn_3 = int(train_data_params["dnn_3"])
        input_shape = (length*2+1,width*2+1, 3)

        model = build_model(input_shape , optimizer , loss_func , learning_rate , dnn_1, dnn_2, dnn_3 )

        log.info("model fitting")
        model.fit(np_x, np_y , epochs = EPOCHS , batch_size  = BATCH_SIZE ,
                  validation_data = (val_x, val_y),
                  callbacks = [callback])


        num_model_dir = os.path.join(saving_model_path, "number_model")
        tf.keras.models.save_model(model, num_model_dir)

        # 생성한 모델을 최적 모델로 적용시 필요한 json 파일 생성
        pixel_legth_width_dic = {"length": length , "width" : width }
        pixel_dic_path   = os.path.join(num_model_dir , "pixel_info_dic.json")
        with open(pixel_dic_path , "w") as fp:
            json.dump(pixel_legth_width_dic , fp)
        fp.close()
